USE [master]
GO

/****** Object:  Database [vinylsCollection]    Script Date: 13.12.2019 20:52:26 ******/
CREATE DATABASE [vinylsCollection]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'vinylsCollection', FILENAME = N'C:\DATA\vinylsCollection.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'vinylsCollection_log', FILENAME = N'C:\DATA\vinylsCollection_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

