

/****************************
*	Bassi Luca				*
*	29.11.2019				*
*							*
*	Creation tables in		*
*	vinylsCollection database		*
*							*
****************************/

USE [vinylsCollection]
GO


DROP TABLE IF EXISTS [dbo].[producers];
CREATE TABLE [dbo].[producers](
	[id] [int] NOT NULL PRIMARY KEY,
	[company] [varchar](100) NULL,
	[street][INT] NULL,
	[nbStreet][INT] NOT NULL,

	[id_city][INT] NOT NULL
) ON [PRIMARY]

DROP TABLE IF EXISTS [dbo].[vinyls];
CREATE TABLE [dbo].[vinyls](
	[id] [int] NOT NULL PRIMARY KEY,
	[name] [varchar](100) NULL,
	[band] [varchar] (100) NULL,
	[nbTitle] [int] NULL,
	[totalDuration] [numeric](5,2) NULL,
	[outDate] [date] NULL,
	[copyNumber] [int] NULL,
	[rpm][INT] NULL,

	[id_producer][int] NOT NULL
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[tracks];
CREATE TABLE [dbo].[tracks](
	[id] [int] NOT NULL PRIMARY KEY,
	[title] [varchar](100) NULL,
	[duration][int] NULL,
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[vinyls_as_tracks];
CREATE TABLE [dbo].[vinyls_as_tracks](
	[id] [int] NOT NULL PRIMARY KEY,

	[id_vinyl][INT] NOT NULL,
	[id_track][INT] NOT NULL
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[singles];
CREATE TABLE [dbo].[singles](
	[id] [int] NOT NULL PRIMARY KEY,
	[name] [varchar](100) NULL,
	[totalDuration] [int] NULL,
	[outDate] [date] NULL,
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[vinyls_as_singles];
CREATE TABLE [dbo].[vinyls_as_singles](
	[id] [int] NOT NULL PRIMARY KEY,

	[id_vinyl][INT] NOT NULL,
	[id_single][INT] NOT NULL
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[artists];
CREATE TABLE [dbo].[artists](
	[id] [int] NOT NULL PRIMARY KEY,
	[artistName] [varchar](100) NULL,
	[firstname] [varchar](100) NULL,
	[lastname] [varchar](100) NULL,
	[bornDate] [date] NULL,

	[id_city] [int] NULL
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[vinyls_as_artists];
CREATE TABLE [dbo].[vinyls_as_artists](
	[id] [int] NOT NULL PRIMARY KEY,

	[id_vinyl][INT] NOT NULL,
	[id_artist][INT] NOT NULL
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[tours];
CREATE TABLE [dbo].[tours](
	[id] [int] NOT NULL PRIMARY KEY,
	[name] [varchar](100) NULL,
	[startDate] [varchar](100) NULL,
	[endDate] [varchar](100) NULL,

	[id_artist][int] NOT NULL
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[concerts];
CREATE TABLE [dbo].[concerts](
	[id] [int] NOT NULL PRIMARY KEY,
	[location] [varchar](100) NULL,
	[date][INT] NULL,

	[id_tour][INT] NOT NULL,
	[id_city][INT] NOT NULL
) ON [PRIMARY]


DROP TABLE IF EXISTS [dbo].[cities];
CREATE TABLE [dbo].[cities](
	[id] [int] NOT NULL PRIMARY KEY,
	[name] [varchar](100) NULL,
	[country] [varchar](100) NULL
) ON [PRIMARY]




ALTER TABLE  [vinyls_as_tracks]ADD FOREIGN KEY (id_vinyl) REFERENCES vinyls(id); 
ALTER TABLE  [vinyls_as_tracks]ADD FOREIGN KEY (id_track) REFERENCES tracks(id);


ALTER TABLE  [vinyls_as_singles]ADD FOREIGN KEY (id_vinyl) REFERENCES vinyls(id); 
ALTER TABLE  [vinyls_as_singles]ADD FOREIGN KEY (id_single) REFERENCES singles(id);

 
ALTER TABLE  [vinyls_as_artists]ADD FOREIGN KEY (id_vinyl) REFERENCES vinyls(id); 
ALTER TABLE  [vinyls_as_artists]ADD FOREIGN KEY (id_artist) REFERENCES singles(id);
 

ALTER TABLE  [artists]ADD FOREIGN KEY (id_city) REFERENCES cities(id); 


ALTER TABLE  [producers]ADD FOREIGN KEY (id_city) REFERENCES cities(id); 


ALTER TABLE  [tours]ADD FOREIGN KEY (id_artist) REFERENCES artists(id); 


ALTER TABLE  [concerts]ADD FOREIGN KEY (id_tour) REFERENCES tours(id); 
ALTER TABLE  [concerts]ADD FOREIGN KEY (id_city) REFERENCES cities(id); 


ALTER TABLE [artists]ADD UNIQUE (firstname, lastname, artistName, bornDate); 


 

GO


